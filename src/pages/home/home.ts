import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { FilePath } from '@ionic-native/file-path';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private file:File,
    private filechooser:FileChooser, private fileopener: FileOpener,
    private filepath: FilePath) {

  }

  openAnyFile(){
    this.filechooser.open().then((fileurl) =>{
    this.filepath.resolveNativePath(fileurl).then ((nativepath)=>{
      let filename=nativepath.substring(nativepath.lastIndexOf('/')+1);
      let folder=nativepath.substring(0,nativepath.lastIndexOf('/')+1);
      this.file.readAsDataURL(folder,filename).then((str)=> {
        var arr= str.split(';');
        var arr1= arr[0].split(':');
        var mimetype=arr1[1];
        this.openFile(nativepath,mimetype);
      })
    })
  })

  }

  openFile(filepath,mimetype){
    this.fileopener.open(filepath,mimetype).then(() => {

    }, (err) => {
      alert(JSON.stringify(err));
    })
  }

}
